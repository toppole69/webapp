# Documentation


## Users

 - [FAQ](./faq.md)
 - [Gitter accounts](./accounts.md)
 - [Mobile apps (Android/iOS)](./mobile-apps.md)
 - [Rooms](./rooms.md)
 - [Communities](./communities.md)
 - [Notifications](./notifications.md)
 - [OAuth Scopes](./oauth-scopes.md)


## Developers

 - [Developer FAQ](./developer-faq.md)
 - [Code overview](./code-overview.md)
 - [Utility scripts](./utility-scripts.md)
 - [`gitter.im` production infrastructure docs](https://gitlab.com/gl-infra/gitter-infrastructure)
